A demonstration of the effects of image lossy vs lossless image compression.

Look in outputs for pictures of Nic Cage changing into other pictures of Nic Cage.

You can also generate your own:
```
docker-compose up -d --build
docker-compose exec morph python3 morph.py
```
