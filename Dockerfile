FROM alpine:edge

RUN apk --no-cache --update upgrade
RUN apk --no-cache --update add \
    python3 \
    py3-pillow \
    ffmpeg

RUN pip3 install --upgrade pip setuptools \
    && rm -rf /root/.cache/pip

ADD requirements.txt /opt/morph/requirements.txt

RUN pip3 install -r /opt/morph/requirements.txt \
  && rm -rf /root/.cache/pip

RUN adduser -D web
USER root

ADD . /opt/morph
WORKDIR /opt
RUN /usr/bin/find . -type d -exec chown web {} \;
WORKDIR /opt/morph
RUN python3 -m compileall -f -q .
USER web
