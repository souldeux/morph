from PIL import Image
import uuid, subprocess, sys, math, os, random, copy

class Manipulator:
    def __init__(self, inputImage, targetImage = None):
        self.inputImage = inputImage
        self.uuid = uuid.uuid4()
        self.size = None
        self.targetImage = self._make_black_mirror() if targetImage is None else targetImage

        im = Image.open(self.targetImage)
        self.size = im.size
        ii = Image.open(self.inputImage)
        tmp = ii.resize(self.size, Image.ANTIALIAS)
        self.inputImage = '/tmp/{}'.format(self.inputImage)
        os.makedirs(os.path.dirname(self.inputImage), exist_ok=True)
        tmp.save(self.inputImage)

    def _make_black_mirror(self):
        im = Image.open(self.inputImage)
        #The default behavior of Image.new is to create a black image - handy!
        mirror = Image.new('RGB', im.size)
        path = '/tmp/black-{}.jpg'.format(self.uuid)
        print ('Saving black mirror of size {} to {}'.format(im.size, path))
        mirror.save(path)
        return path

    def _get_total_pixel_deltas(self, startImage, targetImage):
        imInput = Image.open(startImage)
        imTarget = Image.open(targetImage)

        pxInput = imInput.load()
        pxTarget = imTarget.load()
        deltas = list()
        for x in range(0, self.size[0]):
            row = list()
            for y in range(0, self.size[1]):
                deltaR = pxTarget[x,y][0] - pxInput[x, y][0]
                deltaG = pxTarget[x,y][1] - pxInput[x, y][1]
                deltaB = pxTarget[x,y][2] - pxInput[x, y][2]
                row.append( (deltaR, deltaG, deltaB) )
            deltas.append(row)
        return deltas

    def morph(self, steps, format='jpg'):
        i = 1
        previous = self.inputImage
        target = self.targetImage
        while i < (2*steps)+1:
            totalDeltas = self._get_total_pixel_deltas(previous, target)
            sys.stdout.write("\rMorphing step {} of {} with format {}".format(i, steps*2, format.upper()))
            sys.stdout.flush()
            output = "/tmp/{}-out-{}.{}".format(i, self.uuid, format)

            im = Image.open(previous)
            px = im.load()
            if i not in (1, steps, steps*2):
                for x in range(0, self.size[0]):
                    for y in range(0, self.size[1]):
                        delta = totalDeltas[x][y]
                        _i = i if i < steps else i - steps
                        dR = px[x, y][0] + math.floor(delta[0]/(steps-_i)*2)
                        dG = px[x, y][1] + math.floor(delta[1]/(steps-_i)*2)
                        dB = px[x, y][2] + math.floor(delta[2]/(steps-_i)*2)
                        px[x, y] = (dR, dG, dB)
            im.save(output)
            previous = output
            im.close()
            i += 1
            if i == steps+1:
                #At the halfway point, flip the target
                target = self.inputImage

        #Generate forward run
        output_path = "outputs/{}steps-{}-{}.gif".format(steps, format, self.uuid)

        print("Generating run...")
        cmd = "ffmpeg -f image2 -framerate 30 -pix_fmt pal8 -i /tmp/%d-out-{}.{} {}".format(
            self.uuid, format, output_path
        )
        subprocess.call(cmd, shell=True)


if __name__ == '__main__':
    random.seed(os.urandom(100))
    left = random.randint(1,6)
    right = left
    while right == left:
        right = random.randint(1,6)
    for x in range(120, 360, 60):
        m = Manipulator(
            'assets/{}.jpg'.format(left),
            targetImage='assets/{}.jpg'.format(right)
        )
        m.morph(x)
        m.morph(x, format='bmp')
